﻿using Confluent.Kafka;
using Confluent.Kafka.Serialization;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Timemanagement.Streaming.Producer
{
	public class BookingProducer : IBookingProducer
	{
		public async Task ProduceAsync(string message)
		{
			var config = new Dictionary<string, object>
			{
				{"bootstrap.servers", "localhost:9092" },
				{"enable.auto.commit", "false" },
			};
			using (var producer = new Producer<Null, string>(config, null, new StringSerializer(Encoding.UTF8)))
			{
				await producer.ProduceAsync("timemanagement_booking", null, message);
				producer.Flush(15);
			}
		}
	}
}
