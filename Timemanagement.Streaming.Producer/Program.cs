﻿using System;

namespace Timemanagement.Streaming.Producer
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Send message: > ");
			var message = Console.ReadLine();
			while(message.ToLower() !=  "q")
			{
				var producer = new BookingProducer();
				producer.ProduceAsync(message);
			}
		}
	}
}
