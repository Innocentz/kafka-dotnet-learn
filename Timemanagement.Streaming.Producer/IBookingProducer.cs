﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Timemanagement.Streaming.Producer
{
	public interface IBookingProducer
	{
		Task ProduceAsync(string message);
	}
}
